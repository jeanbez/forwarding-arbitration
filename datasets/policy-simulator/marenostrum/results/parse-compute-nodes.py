import os
import glob
import sys
import csv
import logging

if sys.version_info[0] < 3:
    print('You must use Python 3')

    exit()

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    handlers=[
        logging.FileHandler('parser.log'),
        logging.StreamHandler()
    ]
)

APPLICATIONS = sys.argv[1]
DIRECTORY = '../data/{}-applications/'.format(APPLICATIONS)

# Generate a CSV file with the results
output = csv.writer(open('{}-applications-nodes.csv'.format(APPLICATIONS), 'w'), delimiter=';')

header = [
    'applications',
    'policy',
    'pool_size',
    'total_processes',
    'total_compute_nodes',
    'repetition'
]

output.writerow(header)

# Get all .bash files
experiments = sorted([f for f in glob.glob(DIRECTORY + "*allocation.csv", recursive=True)])

for experiment in experiments:
    logging.info('parsing {}'.format(experiment))

    repetition = experiment.split('-')[3]

    # Open the file and get the configuration
    with open(experiment) as f:
        csv_reader = csv.reader(f, delimiter=';')

        # Skip the header
        next(csv_reader)

        for row in csv_reader:
            total_processes = 0
            total_compute_nodes = 0

            for i in range(0, 16):
                total_processes += int(row[3].split('-')[0])
                total_compute_nodes += int(row[3].split('-')[1])

                try:
                    next(csv_reader)
                except StopIteration:
                    break

            output.writerow([
                row[0],
                row[1],
                row[2],
                total_processes,
                total_compute_nodes,
                repetition
            ])
