"""Process Policy."""

import bisect

from policies.Policy import Policy


class ProcessPolicy(Policy):
    """Implement the zero policy."""

    def set_pool_size(self, size):
        """Define the number of I/O forwarding resources that are avaible in the pool."""
        self.pool_size = size

    def get_processes(self, application):
        """Return the number of processes of the application."""
        return int(application.split('-')[0])

    def apply(self):
        """Apply the policy returning the number of nodes and bandwidth per application."""
        selected_nodes = dict()
        expected_bandwidth = dict()

        # TODO: make this more dynamic
        avaible_forwarders = [0, 1, 2, 4, 8]

        for app in self.applications:
            if '.' in app:
                # Remove the repetition ID of the application and get the bandwidth for that type
                # This allows us to have multiple instaces of the same application
                application = app.split('.')[0]
            else:
                application = app
            maximum_bandwidth = max(self.database[application].items(), key=lambda x: x[1])[1]

            options = []

            for key, value in self.database[application].items():
                if value == maximum_bandwidth:
                    options.append(key)

            expected_bandwidth[app] = float(
                super().database_lookup(application, min(options))
            )
            selected_nodes[app] = min(options)

        # Get the number of node of each application
        compute_nodes = dict()

        for app in self.applications:
            if '.' in app:
                # Remove the repetition ID of the application and get the bandwidth for that type
                # This allows us to have multiple instaces of the same application
                application = app.split('.')[0]
            else:
                application = app

            compute_nodes[app] = self.get_processes(application)
            # print("{}".format(self.get_processes(application)))

        # print(compute_nodes.values())

        # Compute the ratio of which we should give to each application
        total = sum(compute_nodes.values())

        for app in self.applications:
            if '.' in app:
                # Remove the repetition ID of the application and get the bandwidth for that type
                # This allows us to have multiple instaces of the same application
                application = app.split('.')[0]
            else:
                application = app
            # Compute the ratio based on the pool size
            ratio = round(compute_nodes[app] * self.pool_size / total)

            # Round to bottom (always) to the next possible set we have metrics
            selected_nodes[app] = avaible_forwarders[
                bisect.bisect_right(avaible_forwarders, ratio) - 1
            ]

            # Get the bandwidth for that value
            expected_bandwidth[app] = float(
                super().database_lookup(application, selected_nodes[app])
            )

        consider_removing = 1

        # Make sure we are not using more nodes that the ones available in the pool
        while sum(selected_nodes.values()) > self.pool_size:
            self.logger.warning(
                "selected %s nodes, more than the pool size of %s, consider removing %s nodes",
                sum(selected_nodes.values()),
                self.pool_size,
                avaible_forwarders[consider_removing]
            )

            # Find the application with one forwarder and remove it from it
            for app in self.applications:
                if '.' in app:
                    # Remove the repetition ID of the application and get the bandwidth for that type
                    # This allows us to have multiple instaces of the same application
                    application = app.split('.')[0]
                else:
                    application = app

                if selected_nodes[app] == avaible_forwarders[consider_removing]:
                    selected_nodes[app] = avaible_forwarders[consider_removing - 1]
                    expected_bandwidth[app] = float(
                        super().database_lookup(application, selected_nodes[app])
                    )

                    self.logger.warning(
                        "remove %s -> %s nodes from %s (%s nodes in total)",
                        avaible_forwarders[consider_removing],
                        avaible_forwarders[consider_removing - 1],
                        app,
                        sum(selected_nodes.values())
                    )

                    if sum(selected_nodes.values()) <= self.pool_size:
                        self.logger.info(
                            "policy is acceptable now with %s nodes",
                            sum(selected_nodes.values()),
                        )

                        break

            consider_removing += 1

        self.logger.info(
            "policy is acceptable now with %s nodes",
            sum(selected_nodes.values()),
        )

        return (selected_nodes, expected_bandwidth)
