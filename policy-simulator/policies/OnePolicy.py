"""One Policy."""

from policies.Policy import Policy


class OnePolicy(Policy):
    """Implement the zero policy."""

    FORWARDERS = 1

    def apply(self):
        """Apply the policy returning the number of nodes and bandwidth per application."""
        selected_nodes = dict()
        expected_bandwidth = dict()

        for application in self.applications:
            expected_bandwidth[application] = float(
                super().database_lookup(application, self.FORWARDERS)
            )
            selected_nodes[application] = self.FORWARDERS

        return (selected_nodes, expected_bandwidth)
