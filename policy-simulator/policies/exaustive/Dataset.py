"""Dataset for the Multiple-Choice Knapsack Problem."""

from policies.knapsack.Class import Class


class Dataset:
    """Define a dataset with multiple classes of items."""

    def __init__(self, dataset):
        """Initialize the dataset with multiple classes from a three-dimension vector."""
        self._classes = []

        for data in dataset:
            self._classes.append(
                Class(data[0], data[1])
            )

    def get_number_of_classes(self):
        """Return the number of classes in the dataset."""
        return len(self._classes)

    def get_number_of_items_by_class(self):
        """Return a list with the number of items in each class."""
        number_of_items_by_class = []

        for i in range(0, len(self._classes)):
            number_of_items_by_class.append(self._classes[i].get_number_of_items())

        return number_of_items_by_class

    def get_classes(self):
        """Return the classes."""
        return self._classes

    def display(self):
        """Display information about the dataset."""
        for i in range(0, len(self._classes)):
            print("Class {}:".format(i + 1))
            self._classes[i].display()
            print("\n")

    def get_min_weight(self):
        """Return the minimum weight in the dataset."""
        total = 0

        for i in range(0, len(self._classes)):
            total += self._classes[i].get_min_weight()

        return total

    def get_max_weight(self):
        """Return the maximum weight in the dataset."""
        total = 0

        for i in range(0, len(self._classes)):
            total += self._classes[i].get_max_weight()

        return total

    def __getitem__(self, index):
        """Return one of the classes from the dataset."""
        return self._classes[index]
