"""Items for the Multiple-Choice Knapsack Problem."""


class Item:
    """Define a item."""

    __created_items = 0

    def __init__(self, value, weight):
        """Initialize a new item with a given value and weight."""
        self._value = value
        self._weight = weight

        Item.__created_items += 1
        self._index = Item.__created_items

    def get_value(self):
        """Return the value of the item."""
        return self._value

    def get_weight(self):
        """Return the weight of the item."""
        return self._weight

    def get_index(self):
        """Return the index of the item."""
        return self._index

    def get_created_items(self):
        """Return the number of created items."""
        return self.__created_items

    def display(self):
        """Display information about the item."""
        print("Item {}, weight {}, value {}".format(self._index, self._weight, self._value))
