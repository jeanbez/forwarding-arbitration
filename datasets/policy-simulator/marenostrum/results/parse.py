import os
import glob
import sys
import csv
import logging

if sys.version_info[0] < 3:
    print('You must use Python 3')

    exit()

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    handlers=[
        logging.FileHandler('parser.log'),
        logging.StreamHandler()
    ]
)

APPLICATIONS = sys.argv[1]
DIRECTORY = '../data/{}-applications/'.format(APPLICATIONS)

# Generate a CSV file with the results
output = csv.writer(open('{}-applications-results.csv'.format(APPLICATIONS), 'w'), delimiter=';')

header = [
    'applications',
    'policy',
    'time_to_schedule',
    'available_forwarders',
    'allocated_forwarders',
    'global_bandwidth',
    'repetition'
]

output.writerow(header)

# Get all .bash files
experiments = sorted([f for f in glob.glob(DIRECTORY + "*results.csv", recursive=True)])

for experiment in experiments:
    logging.info('parsing {}'.format(experiment))

    repetition = experiment.split('-')[3]

    # Open the file and get the configuration
    with open(experiment) as f:
        csv_reader = csv.reader(f, delimiter=';')

        # Skip the header
        next(csv_reader)

        for row in csv_reader:
            row.append(repetition)

            output.writerow(row)
