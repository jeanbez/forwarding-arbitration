"""Policy."""

import logging
import logging.handlers


class Policy:
    """Implement one policy."""

    LOG_FILENAME = 'simulation.log'

    database = None
    applications = None

    def __init__(self, database, debug, log_file):
        """Initialize the policy class."""
        self.set_database(database)
        self.LOG_FILENAME = log_file
        self.configure_log(debug)

    def set_applications(self, applications):
        """Set the applications we are going to consider in the policy."""
        self.applications = applications

    def set_database(self, database):
        """Set the database that we are going to use to make decisions."""
        self.database = database

    def database_lookup(self, application, forwarders):
        """Look up the expected bandwidth  for a given application and number of forwarders."""
        if '.' in application:
            # Remove the repetition ID of the application and get the bandwidth for that type
            # This allows us to have multiple instaces of the same application
            application = application.split('.')[0]

        if application in self.database and forwarders in self.database[application]:
            return self.database[application][forwarders]

        return -1

    def configure_log(self, debug):
        """Configure the logging system."""
        self.logger = logging.getLogger('Policy')

        if debug:
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger.setLevel(logging.INFO)

        # Defines the format of the logger
        formatter = logging.Formatter('%(asctime)s %(module)s - %(levelname)s - %(message)s')

        # Configure the log rotation
        handler = logging.handlers.RotatingFileHandler(
            self.LOG_FILENAME,
            maxBytes=268435456,
            backupCount=50,
            encoding='utf8'
        )

        handler.setFormatter(formatter)

        self.logger.addHandler(handler)

    def apply(self):
        """Apply the policy returning the number of nodes and bandwidth per application."""
        pass

