"""Exhaustive Policy."""

import bisect
import itertools
import pprint

from policies.Policy import Policy


class ExhaustivePolicy(Policy):
    """Implement the zero policy."""

    def set_pool_size(self, size):
        """Define the number of I/O forwarding resources that are avaible in the pool."""
        self.pool_size = size

    def apply(self):
        """Apply the policy returning the number of nodes and bandwidth per application."""
        selected_nodes = dict()
        expected_bandwidth = dict()

        expected_bandwidth_total = 0

        # TODO: make this more dynamic
        avaible_forwarders = [0, 1, 2, 4, 8]

        options = [p for p in itertools.product(avaible_forwarders, repeat=len(self.applications))]

        # list(itertools.permutations(avaible_forwarders, len(self.applications)))
        # pprint.pprint(options)

        for option in options:
            if sum(list(option)) <= self.pool_size:
                current_option = dict()
                current_option_bandwidth = dict()

                current_option_total = 0

                # Compute the total expected badwidth
                for i, application in enumerate(self.applications):
                    current_option[application] = option[i]
                    current_option_bandwidth[application] = float(
                        super().database_lookup(application, option[i])
                    )

                    current_option_total += current_option_bandwidth[application]

                if current_option_total > expected_bandwidth_total:
                    selected_nodes = current_option
                    expected_bandwidth = current_option_bandwidth

                    expected_bandwidth_total = current_option_total

        # pprint.pprint(valid_options)
        # pprint.pprint(selected_nodes)
        # pprint.pprint(expected_bandwidth)

        if sum(selected_nodes.values()) > self.pool_size:
            self.logger.error(
                "selected %s nodes, more than the pool size of %s",
                sum(selected_nodes.values()),
                self.pool_size
            )

        return (selected_nodes, expected_bandwidth)
