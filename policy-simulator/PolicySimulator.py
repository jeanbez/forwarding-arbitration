"""Policy Simulator."""

import os
import csv
import json
import datetime
import pprint
import logging
import logging.handlers

from policies.ZeroPolicy import ZeroPolicy
from policies.OnePolicy import OnePolicy
from policies.StaticPolicy import StaticPolicy
from policies.SizePolicy import SizePolicy
from policies.ProcessPolicy import ProcessPolicy
from policies.OptimalPolicy import OptimalPolicy
from policies.MultipleChoiceKnapsackPolicy import MultipleChoiceKnapsackPolicy
from policies.ExhaustivePolicy import ExhaustivePolicy


class PolicySimulator:
    """Simulate multiple I/O forwarding scheduling policies."""

    DB_BANDWIDTH_FILE = 'database/bandwidths.csv'
    DB_BANDWIDTH = dict()

    MAXIMUM_FORWARDERS = 0

    def __init__(self, setup, debug, exhaustive):
        """Initialize the simulator."""
        self.LOG_FILENAME = 'logs/{}-simulation.log'.format(setup.replace('.json', ''))

        self.configure_log(debug)
        self.load_database()

        # Handle the exhaustive search policy
        self.exhaustive = exhaustive

        self.load_policies(debug)

        self.setup = setup

    def configure_log(self, debug):
        """Configure the logging system."""
        self.logger = logging.getLogger('PolicySimulator')

        if debug:
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger.setLevel(logging.INFO)

        # Defines the format of the logger
        formatter = logging.Formatter('%(asctime)s %(module)s - %(levelname)s - %(message)s')

        # Configure the log rotation
        handler = logging.handlers.RotatingFileHandler(
            self.LOG_FILENAME,
            maxBytes=268435456,
            backupCount=50,
            encoding='utf8'
        )

        handler.setFormatter(formatter)

        self.logger.addHandler(handler)

        self.logger.info('Starting Policy Simulator')

    def load_database(self):
        """Load the database of access patterns and performance metrics."""
        if not os.path.isfile(self.DB_BANDWIDTH_FILE):
            self.logger.error('unable to find the bandwidth database file')

            exit()

        with open(self.DB_BANDWIDTH_FILE, 'r') as csv_file:
            rows = csv.DictReader(csv_file, delimiter=',')

            for row in rows:
                if row['scenario'] not in self.DB_BANDWIDTH:
                    self.DB_BANDWIDTH[row['scenario']] = dict()

                self.DB_BANDWIDTH[row['scenario']][int(row['forwarders'])] = float(row['bandwidth'])

                self.MAXIMUM_FORWARDERS = max(self.MAXIMUM_FORWARDERS, int(row['forwarders']))

        self.logger.info('loaded database of bandwidths')

    def load_policies(self, debug):
        """Load all the available policies and configure them accordingly."""
        self.zero_policy = ZeroPolicy(self.DB_BANDWIDTH, debug, self.LOG_FILENAME)
        self.one_policy = OnePolicy(self.DB_BANDWIDTH, debug, self.LOG_FILENAME)
        self.static_policy = StaticPolicy(self.DB_BANDWIDTH, debug, self.LOG_FILENAME)
        self.size_policy = SizePolicy(self.DB_BANDWIDTH, debug, self.LOG_FILENAME)
        self.process_policy = ProcessPolicy(self.DB_BANDWIDTH, debug, self.LOG_FILENAME)
        self.optimal_policy = OptimalPolicy(self.DB_BANDWIDTH, debug, self.LOG_FILENAME)
        self.knapsack_policy = MultipleChoiceKnapsackPolicy(self.DB_BANDWIDTH, debug, self.LOG_FILENAME)
        if self.exhaustive:
            self.exhaustive_policy = ExhaustivePolicy(self.DB_BANDWIDTH, debug, self.LOG_FILENAME)

    def run(self):
        """Simulate the allocation policy."""
        # Read a JSON file with the patterns to simulate
        with open(self.setup) as file:
            setup = json.load(file)

        applications = setup['patterns']

        row = [
            'applications',
            'policy',
            'time_to_schedule',
            'available_forwarders',
            'allocated_forwarders',
            'global_bandwidth'
        ]

        self.logger.info('loaded applications/patterns')

        csv_file_name = self.setup.replace('.json', '-results.csv')

        csv_file = open(csv_file_name, mode='w')
        csv_writer = csv.writer(
            csv_file,
            delimiter=';',
            quoting=csv.QUOTE_MINIMAL
        )
        csv_writer.writerow(row)

        row = [
            'applications',
            'policy',
            'available_forwarders',
            'application',
            'allocated_forwarders',
            'expected_bandwidth'
        ]

        csv_file_allocation_name = self.setup.replace('.json', '-allocation.csv')

        csv_file_allocation = open(csv_file_allocation_name, mode='w')
        csv_writer_allocation = csv.writer(
            csv_file_allocation,
            delimiter=';',
            quoting=csv.QUOTE_MINIMAL
        )
        csv_writer_allocation.writerow(row)

        self.logger.info('simulate zero policy')

        # Simulate the policies that are in the JSON (and exists) according to their arguments
        self.zero_policy.set_applications(applications)
        time_s = datetime.datetime.now()
        (selected_nodes, expected_bandwidth) = self.zero_policy.apply()
        time_f = datetime.datetime.now()

        time_delta = time_f - time_s

        elapsed = int(time_delta.total_seconds() * 1000000)  # microseconds

        row = [
            len(applications),
            'ZERO',
            elapsed,
            None,
            sum(selected_nodes.values()),
            sum(expected_bandwidth.values())
        ]
        csv_writer.writerow(row)

        for application in applications:
            row = [
                len(applications),
                'ZERO',
                None,
                application,
                selected_nodes[application],
                expected_bandwidth[application]
            ]

            csv_writer_allocation.writerow(row)

        # pprint.pprint(selected_nodes)
        # pprint.pprint(expected_bandwidth)

        self.logger.info('simulate one policy')

        self.one_policy.set_applications(applications)
        time_s = datetime.datetime.now()
        (selected_nodes, expected_bandwidth) = self.one_policy.apply()
        time_f = datetime.datetime.now()

        time_delta = time_f - time_s

        elapsed = int(time_delta.total_seconds() * 1000000)  # microseconds

        row = [
            len(applications),
            'ONE',
            elapsed,
            None,
            sum(selected_nodes.values()),
            sum(expected_bandwidth.values())
        ]
        csv_writer.writerow(row)

        for application in applications:
            row = [
                len(applications),
                'ONE',
                None,
                application,
                selected_nodes[application],
                expected_bandwidth[application]
            ]

            csv_writer_allocation.writerow(row)

        # pprint.pprint(selected_nodes)
        # pprint.pprint(expected_bandwidth)

        self.logger.info('simulate optimal policy')

        self.optimal_policy.set_applications(applications)
        time_s = datetime.datetime.now()
        (selected_nodes, expected_bandwidth) = self.optimal_policy.apply()
        time_f = datetime.datetime.now()

        time_delta = time_f - time_s

        elapsed = int(time_delta.total_seconds() * 1000000)  # microseconds

        row = [
            len(applications),
            'OPTIMAL',
            elapsed,
            None,
            sum(selected_nodes.values()),
            sum(expected_bandwidth.values())
        ]
        csv_writer.writerow(row)

        for application in applications:
            row = [
                len(applications),
                'OPTIMAL',
                None,
                application,
                selected_nodes[application],
                expected_bandwidth[application]
            ]

            csv_writer_allocation.writerow(row)

        # pprint.pprint(selected_nodes)
        # pprint.pprint(expected_bandwidth)

        for pool_size in range(0, (len(applications) * self.MAXIMUM_FORWARDERS) + 1):
            self.logger.info('simulate static policy (pool = %s)', pool_size)

            # Simulate the policies that are in the JSON (and exists) according to their arguments
            self.static_policy.set_applications(applications)
            self.static_policy.set_pool_size(pool_size)

            # We need to provice the number of total compute nodes for the static policy
            machine_size = 0
            for application in applications:
                machine_size += self.static_policy.get_compute_nodes(application)

            self.logger.info('machine size (compute node = %s)', machine_size)

            self.static_policy.set_machine_size(machine_size)

            time_s = datetime.datetime.now()
            (selected_nodes, expected_bandwidth) = self.static_policy.apply()
            time_f = datetime.datetime.now()

            time_delta = time_f - time_s

            elapsed = int(time_delta.total_seconds() * 1000000)  # microseconds

            row = [
                len(applications),
                'STATIC',
                elapsed,
                pool_size,
                sum(selected_nodes.values()),
                sum(expected_bandwidth.values())
            ]
            csv_writer.writerow(row)

            for application in applications:
                row = [
                    len(applications),
                    'STATIC',
                    pool_size,
                    application,
                    selected_nodes[application],
                    expected_bandwidth[application]
                ]

                csv_writer_allocation.writerow(row)

            # pprint.pprint(selected_nodes)
            # pprint.pprint(expected_bandwidth)

        for pool_size in range(0, (len(applications) * self.MAXIMUM_FORWARDERS) + 1):
            self.logger.info('simulate size policy (pool = %s)', pool_size)

            # Simulate the policies that are in the JSON (and exists) according to their arguments
            self.size_policy.set_applications(applications)
            self.size_policy.set_pool_size(pool_size)

            time_s = datetime.datetime.now()
            (selected_nodes, expected_bandwidth) = self.size_policy.apply()
            time_f = datetime.datetime.now()

            time_delta = time_f - time_s

            elapsed = int(time_delta.total_seconds() * 1000000)  # microseconds

            row = [
                len(applications),
                'SIZE',
                elapsed,
                pool_size,
                sum(selected_nodes.values()),
                sum(expected_bandwidth.values())
            ]
            csv_writer.writerow(row)

            for application in applications:
                row = [
                    len(applications),
                    'SIZE',
                    pool_size,
                    application,
                    selected_nodes[application],
                    expected_bandwidth[application]
                ]

                csv_writer_allocation.writerow(row)

            # pprint.pprint(selected_nodes)
            # pprint.pprint(expected_bandwidth)

        for pool_size in range(0, (len(applications) * self.MAXIMUM_FORWARDERS) + 1):
            self.logger.info('simulate process policy (pool = %s)', pool_size)

            # Simulate the policies that are in the JSON (and exists) according to their arguments
            self.process_policy.set_applications(applications)
            self.process_policy.set_pool_size(pool_size)

            time_s = datetime.datetime.now()
            (selected_nodes, expected_bandwidth) = self.process_policy.apply()
            time_f = datetime.datetime.now()

            time_delta = time_f - time_s

            elapsed = int(time_delta.total_seconds() * 1000000)  # microseconds

            row = [
                len(applications),
                'PROCESS',
                elapsed,
                pool_size,
                sum(selected_nodes.values()),
                sum(expected_bandwidth.values())
            ]
            csv_writer.writerow(row)

            for application in applications:
                row = [
                    len(applications),
                    'PROCESS',
                    pool_size,
                    application,
                    selected_nodes[application],
                    expected_bandwidth[application]
                ]

                csv_writer_allocation.writerow(row)

            # pprint.pprint(selected_nodes)
            # pprint.pprint(expected_bandwidth)

        for pool_size in range(0, (len(applications) * self.MAXIMUM_FORWARDERS) + 1):
            self.logger.info('simulate multiple-choice knapsack policy (pool = %s)', pool_size)

            self.knapsack_policy.set_applications(applications)
            self.knapsack_policy.set_pool_size(pool_size)

            time_s = datetime.datetime.now()
            (selected_nodes, expected_bandwidth) = self.knapsack_policy.apply()
            time_f = datetime.datetime.now()

            time_delta = time_f - time_s

            elapsed = int(time_delta.total_seconds() * 1000000)  # microseconds

            row = [
                len(applications),
                'MCKP',
                elapsed,
                pool_size,
                sum(selected_nodes.values()),
                sum(expected_bandwidth.values())
            ]
            csv_writer.writerow(row)

            for application in applications:
                row = [
                    len(applications),
                    'MCKP',
                    pool_size,
                    application,
                    selected_nodes[application],
                    expected_bandwidth[application]
                ]

                csv_writer_allocation.writerow(row)

            # pprint.pprint(selected_nodes)
            # pprint.pprint(expected_bandwidth)

        if self.exhaustive:
            for pool_size in range(0, (len(applications) * self.MAXIMUM_FORWARDERS) + 1):
                self.logger.info('simulate exhaustive policy (pool = %s)', pool_size)

                self.exhaustive_policy.set_applications(applications)
                self.exhaustive_policy.set_pool_size(pool_size)

                time_s = datetime.datetime.now()
                (selected_nodes, expected_bandwidth) = self.exhaustive_policy.apply()
                time_f = datetime.datetime.now()

                time_delta = time_f - time_s

                elapsed = int(time_delta.total_seconds() * 1000000)  # microseconds

                row = [
                    len(applications),
                    'EXHAUSTIVE',
                    elapsed,
                    pool_size,
                    sum(selected_nodes.values()),
                    sum(expected_bandwidth.values())
                ]
                csv_writer.writerow(row)

                for application in applications:
                    row = [
                        len(applications),
                        'EXHAUSTIVE',
                        pool_size,
                        application,
                        selected_nodes[application],
                        expected_bandwidth[application]
                    ]

                    csv_writer_allocation.writerow(row)

                # pprint.pprint(selected_nodes)
                # pprint.pprint(expected_bandwidth)

        csv_file.close()

        self.logger.info('done')

