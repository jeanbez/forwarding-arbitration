"""Defined allocation by the Multiple-Choice Knapsack Problem."""


class Allocation:
    """Define an allocation of items, i.e., a solution to the problem."""

    def __init__(self, items):
        """Define a new allocation given the items."""
        self.__items = items

    def change_item(self, class_index, new_item):
        """Change an item in the allocation given the index."""
        self.__items[class_index] = new_item

    def display(self):
        """Display the selected items in the allocation."""
        print('SOLUTION ----------------------------------------------')

        for i in range(0, len(self.__items)):
            print("Class {}: ".format(i + 1))

            self.__items[i].display()

    def get_weights(self):
        """Return a list of weights of the selected items in the allocation."""
        weights = []

        for i in range(0, len(self.__items)):
            weights.append(self.__items[i].get_weight())

        return weights

    def get_values(self):
        """Return a list of values of the selected items in the allocation."""
        values = []

        for i in range(0, len(self.__items)):
            values.append(self.__items[i].get_value())

        return values

    def get_weight(self):
        """Return the total weight of the selected items in the allocation."""
        weight = 0

        for i in range(0, len(self.__items)):
            weight += self.__items[i].get_weight()

        return weight

    def get_value(self):
        """Return the total value of the selected items in the allocation."""
        value = 0

        for i in range(0, len(self.__items)):
            value += self.__items[i].get_value()

        return value
