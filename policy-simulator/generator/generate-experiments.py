import os
import csv
import json
import random


DB_BANDWIDTH_FILE = '../database/bandwidths.csv'
DB_BANDWIDTH = dict()

APPLICATIONS = 16
REPETITIONS = 10000

"""Load the database of access patterns and performance metrics."""
if not os.path.isfile(DB_BANDWIDTH_FILE):
    print('unable to find the bandwidth database file')

    exit()

with open(DB_BANDWIDTH_FILE, 'r') as csv_file:
    rows = csv.DictReader(csv_file, delimiter=',')

    for row in rows:
        if row['scenario'] not in DB_BANDWIDTH:
            DB_BANDWIDTH[row['scenario']] = dict()

        DB_BANDWIDTH[row['scenario']][int(row['forwarders'])] = float(row['bandwidth'])


print('available options: {}'.format(len(DB_BANDWIDTH.keys())))

random.seed(241192)

# Randomly select APPLICAtIONS to compose the JSON
for i in range(0, REPETITIONS):
    with open('experiments/{}-applications-{:05d}.json'.format(APPLICATIONS, i), 'w') as json_file:
        # We can have duplicate patterns
        configuration = dict()
        configuration['patterns'] = (random.choices(list(DB_BANDWIDTH.keys()), k=APPLICATIONS))

        for i, pattern in enumerate(configuration['patterns']):
            configuration['patterns'][i] = '{}.{}'.format(pattern, i)

        json.dump(
            configuration,
            json_file,
            sort_keys=False,
            indent=4
        )
