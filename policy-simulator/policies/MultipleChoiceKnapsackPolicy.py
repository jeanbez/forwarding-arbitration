"""MultipleChoiceKnapsack Policy."""

import numpy
import pprint

from policies.Policy import Policy


class MultipleChoiceKnapsackPolicy(Policy):
    """Implement the zero policy."""

    def set_pool_size(self, size):
        """Define the number of I/O forwarding resources that are avaible in the pool."""
        self.pool_size = size

    def apply(self):
        """Apply the policy returning the number of nodes and bandwidth per application."""
        selected_nodes = dict()
        expected_bandwidth = dict()

        # TODO: make this more dynamic
        avaible_forwarders = [0, 1, 2, 4, 8]

        values = {}
        weight = {}
        groups = {}

        for group_id, application in enumerate(self.applications):
            values[group_id] = []
            weight[group_id] = []
            groups[group_id] = []

            for nodes in avaible_forwarders:
                # Since we need to use integers and we are using five precision points, convert it
                values[group_id].append(int(self.database_lookup(application, nodes) * 100000.0))
                weight[group_id].append(nodes)

        # pprint.pprint(values)
        # pprint.pprint(weight)

        table = numpy.zeros(
            (len(self.applications), self.pool_size + 1),
            dtype=int
        )
        solution_table = numpy.zeros(
            (len(self.applications), len(avaible_forwarders), self.pool_size + 1),
            dtype=int
        )

        # print("CLASS {}".format(0))

        for i in range(0, len(avaible_forwarders)):
            if weight[0][i] <= self.pool_size:
                table[0][weight[0][i]] = max(table[0][weight[0][i]], values[0][i])

                solution_table[0][i][0] = table[0][weight[0][i]]

        # pprint.pprint(table)
        # pprint.pprint(solution_table)

        for j in range(0, len(avaible_forwarders)):
            for k in range(0, self.pool_size + 1):
                if k > 0:
                    solution_table[0][j][k] = solution_table[0][j][0]

        # pprint.pprint(table)
        # pprint.pprint(solution_table)

        for i in range(1, len(self.applications)):
            # print("CLASS {}".format(i))

            for j in range(0, len(avaible_forwarders)):
                for k in range(0, self.pool_size + 1):
                    if k < weight[i][j]:
                        solution_table[i][j][k] = solution_table[i][j - 1][k]
                    elif table[i - 1][k - weight[i][j]] > 0:
                        table[i][k] = max(
                            table[i][k],
                            table[i - 1][k - weight[i][j]] + values[i][j]
                        )

                        solution_table[i][j][k] = table[i][k]
                    else:
                        solution_table[i][j][k] = table[i][k]

            # pprint.pprint(solution_table)

        index_max = 0
        index_max_solution = 0

        for i in range(0, self.pool_size + 1):
            if index_max_solution <= solution_table[len(weight) - 1][len(avaible_forwarders) - 1][i]:
                index_max_solution = solution_table[len(weight) - 1][len(avaible_forwarders) - 1][i]
                index_max = i

        remaining = solution_table[len(weight) - 1][len(avaible_forwarders) - 1][index_max]

        # print(remaining)

        w = index_max
        class_id = len(weight) - 1
        i = len(avaible_forwarders) - 1

        allocated_nodes = dict()
        allocated_bandwidth = dict()

        while remaining > 0:
            # print(" still remaining... {} (w = {}) (class = {}) (i = {}) (solution = {})\n".format(remaining, w, class_id, i, solution_table[class_id][i][w]))

            if i > 0 and remaining == solution_table[class_id][i-1][w]:
                # print(" remaining (%d) == (%d) SOL[class_id][i-1][k]\n", remaining, SOL[class_id][i-1][w])
                # printf(" skipping....\n");
                i = i - 1
            else:
                # The item belongs to the solution
                # print("[{}] class = {}, w = {}, value = {}\n".format(weight[class_id][i], class_id, w, values[class_id][i]))

                allocated_nodes[class_id] = weight[class_id][i]
                allocated_bandwidth[class_id] = values[class_id][i]

                # Redeuce the weight of the item from the pending total weight
                remaining = remaining - values[class_id][i]
                w = w - weight[class_id][i]

                # Since we can only pick ONE item from each class, move on to the next one
                class_id -= 1

                i = min(w, len(avaible_forwarders) - 1)

                if i == 0:
                    i = len(avaible_forwarders) - 1

                if w == 0:
                    # In case we do not have more available forwarders, we need to use 0
                    i = 0

        # total = sum(allocated_nodes.values())
        # print(allocated_nodes)

        for i, application in enumerate(self.applications):
            selected_nodes[application] = allocated_nodes[i]
            # Remember to convert the value back to the floating point
            expected_bandwidth[application] = allocated_bandwidth[i] / 100000.0

        return (selected_nodes, expected_bandwidth)
