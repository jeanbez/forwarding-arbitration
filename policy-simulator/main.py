"""Simulate different I/O forwarding allocation policies."""

import argparse

from PolicySimulator import PolicySimulator

# Configure the parser
PARSER = argparse.ArgumentParser()

PARSER.add_argument(
    '--setup',
    action='store',
    dest='setup',
    required=True,
    help='JSON file with the applications/pattern to simulate')

PARSER.add_argument(
    '--debug',
    action='store_true',
    dest='debug',
    help='Enable debug mode')

PARSER.add_argument(
    '--exhaustive',
    action='store_true',
    dest='exhaustive',
    help='Enable exhaustive policy')

# Parse the arguments
ARGS = PARSER.parse_args()

SIMULATOR = PolicySimulator(ARGS.setup, ARGS.debug, ARGS.exhaustive)
SIMULATOR.run()

