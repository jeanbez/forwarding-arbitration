"""Optimal Policy."""

from policies.Policy import Policy


class OptimalPolicy(Policy):
    """Implement the zero policy."""

    def apply(self):
        """Apply the policy returning the number of nodes and bandwidth per application."""
        selected_nodes = dict()
        expected_bandwidth = dict()

        for app in self.applications:
            if '.' in app:
                # Remove the repetition ID of the application and get the bandwidth for that type
                # This allows us to have multiple instaces of the same application
                application = app.split('.')[0]
            else:
                application = app

            maximum_bandwidth = max(self.database[application].items(), key=lambda x: x[1])[1]

            options = []

            for key, value in self.database[application].items():
                if value == maximum_bandwidth:
                    options.append(key)

            expected_bandwidth[app] = float(
                super().database_lookup(application, min(options))
            )
            selected_nodes[app] = min(options)

        return (selected_nodes, expected_bandwidth)
