"""Class of items for the Multiple-Choice Knapsack Problem."""

from functools import cmp_to_key
from policies.knapsack.Item import Item


class Class:
    """Define a class of items."""

    def __init__(self, values, weights):
        """Initialize a class of items from the vector of items (values and weights)."""
        self._item = []

        for j in range(0, min(len(values), len(weights))):
            self._item.append(Item(values[j], weights[j]))

    def set_items(self, items):
        """Define the items of the class."""
        self._item = items

    def get_number_of_items(self):
        """Return the number of items in class."""
        return len(self._item)

    def get_items(self):
        """Return the items in this class."""
        return self._item

    def get_item(self, index):
        """Return the item in this class based on the index."""
        return self._item[index]

    def sort_items(self, item_a, item_b):
        """Sort the two items A and B."""
        if item_a is not None and item_b is not None:
            if item_a.get_weight() != item_b.get_weight():
                compare = item_a.get_weight() < item_b.get_weight()
            else:
                compare = item_a.get_value() > item_b.get_value()

            return compare

        return False

    def display(self):
        """Display the items in the class."""
        for item in self._item:
            item.display()

    def get_min_weight(self):
        """Return the minimum weight of all the items in the class."""
        minimum = self._item[0].get_weight()

        number_of_items = len(self._item)

        if number_of_items > 1:
            for j in range(1, number_of_items):
                if self._item[j].get_weight() < minimum:
                    minimum = self._item[j].get_weight()

        return minimum

    def get_max_weight(self):
        """Return the maximum weight of all the items in the class."""
        maximum = self._item[0].get_weight()

        number_of_items = len(self._item)

        if number_of_items > 1:
            for j in range(1, number_of_items):
                if self._item[j].get_weight() > maximum:
                    maximum = self._item[j].get_weight()

        return maximum

    def find_most_efficient_replacer(self, current):
        """Return an item of the clas which is the most efficient replacer."""
        res = None
        max_efficiency = 0.0

        for i in self.get_items():
            if i.get_index() != current.get_index():
                new_efficiency = (
                    (i.get_value() - current.get_value()) / (i.get_weight() - current.get_weight())
                )

                if res is not None:  # if res already points toward an Item of the class
                    if max_efficiency < new_efficiency:
                        max_efficiency = new_efficiency
                        res = i
                else:
                    res = i
                    max_efficiency = new_efficiency

        return (res, max_efficiency)  # if res equals nullptr, then @it is alone in the class

    def eliminate_dominated_items(self):
        """Eliminate the dominated items in the class."""
        self._item = sorted(self._item, key=cmp_to_key(self.sort_items))

        res = []
        res.append(self._item[0])

        for k in range(1, self.get_number_of_items()):
            is_dominated = False

            j = 0
            while not is_dominated and j < k:
                is_dominated = (
                    self._item[j].get_weight() <= self._item[k].get_weight() and
                    self._item[j].get_value() >= self._item[k].get_value()
                )
                j = j + 1

            if not is_dominated:
                res.append(self._item[k])

        new = Class([], [])
        new.set_items(res)

        return new

    def delete_items(self, indices):
        """Delete item given their index."""
        for i in sorted(indices, reverse=True):
            del self._item[i]

    def free_items(self):
        """Free all the items of this class."""
        self._item = []
